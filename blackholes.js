
const zero = new SuperPlayer('Zero', 1800, 1690, 30, 10, 10, -45, 'Zero', 'zerohealth')
const ken = new Player('Ken', 1000, 1525, 30, 10, 10, 0, 'Ken', 'kenhealth')

let idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45), 150 }, 150)
let idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 3975, 0), 100, 150 }, 150)

// const projectile = new Projectile ()

const moveChoices = {
    // 'ArrowUp': () => zero.move(),
    'ArrowRight': () => {
        clearInterval(idleInterval)
        zero.move(+1, -340.5, -5107.5, -681, -4100)
        idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45) }, 150)
    },
    // 'ArrowDown': () => zero.move(+1),
    'ArrowLeft': () => {
        clearInterval(idleInterval)
        zero.move(-1, -340.5, -5107.5, -681, -4100)
        idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45) }, 150)
    },
    // 'w': () => ken.move(),
    'd': () => {
        clearInterval(idleInterval2)
        ken.move(+1, -690.5, -1900.5, -420, -500)
        idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 0, 0) }, 150)
    },
    // 's': () => ken.move(),
    'a': () => {
        clearInterval(idleInterval2)
        ken.move(-1, -690.5, -1900.5, -420, -500)
        idleInterval2 = setInterval(function () { stanceZero(ken, 400, -3388, 0, 0) }, 150)
    },
    'x': () => ken.attackMove(zero),

    'Shift': () => {
        clearInterval(idleInterval)
        zero.attackMove(ken)

        
    },
    'z': () => ken.attackMove(zero)




}
function stanceZero(Character, xChange, xEnd, yStart, xStart) {
    if (Character.idleStart > xEnd) {
        Character.idleStart -= xChange
        Character.element.style.backgroundPosition = Character.idleStart + `px ${yStart}px`;
    } else if (Character.idleStart <= xEnd) {
        Character.idleStart = xStart
    }
}
document.addEventListener('keydown', (e) => {
    moveChoices[e.key]()
})










var battling = true;
var playerHp = 20;
var enemyHp = 100;

//Player's Attack Turn
var playerTurn = function () {
    var playerAccuracy = Math.floor(Math.random() * 3 + 1);
    var playerAttackDamage = Math.floor(Math.random() * 20 + 1);

    if (playerAccuracy > 1) {
        enemyHp -= playerAttackDamage;
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("Zero uses SlashStorm.  Ken suffers" + " " + playerAttackDamage + " " + "damage!\nZero:" + " " + playerHp + "HP\nKen:" + " " + enemyHp + "HP"));
        // document.getElementById("container").appendChild(div1).style.position = "fixed|relative|"
        document.getElementById("fightLog").appendChild(div1).style.size = 1 + "px"
        container.style.color = "red";
    } else {
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("Your attack missed!"));
        document.getElementById("fightLog").appendChild(div1)
    }
};


//Enemy's Attack Turn
var enemyTurn = function () {
    var enemyAccuracy = Math.floor(Math.random() * 5 + 1);
    var enemyAttackDamage = Math.floor(Math.random() * 5 + 1);

    if (enemyAccuracy > 3) {
        let div1 = document.createElement("p")
        playerHp -= enemyAttackDamage;
        div1.appendChild(document.createTextNode("Ken uses ShinkuHADOKEN!  You suffer" + " " + enemyAttackDamage + " " + "damage!\nZero:" + " " + playerHp + "HP\nKen:" + " " + enemyHp + "HP"));
        document.getElementById("fightLog").appendChild(div1)
        // container.div2.position = center;
    } else {
        let div1 = document.createElement("p")
        div1.appendChild(document.createTextNode("ken misses!"));
        document.getElementById("fightLog").appendChild(div1)
    }
};

var battle = function () {
    console.log("MentalBattle");
    let div1 = document.createElement("p")
    while (battling) {
        playerTurn();
        if (enemyHp <= 0) {
            div1.appendChild(document.createTextNode("Victory!"));
            document.getElementById("fightLog").appendChild(div1)
            battling = false;
        }
        if (enemyHp > 0) {
            enemyTurn();
            if (playerHp <= 0) {
                let div1 = document.createElement("p")
                div1.appendChild(document.createTextNode("Defeat!"));
                document.getElementById("fightLog").appendChild
                battling = false;
            }
        }
    }
};
battle();





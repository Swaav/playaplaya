class Player {
    constructor(name, x, y, velX, velY, size, idleStart, id, healthElementId) {
        this.name = name;
        this.x = x;
        this.y = y;
        this.velX = velX;
        this.velY = velY;
        this.size = size;
        this.idleStart = idleStart;
        this.element = document.getElementById(id);
        this.healthElement = document.getElementById(healthElementId);
        this.moveStart = 0;
        this.hp = 100;
    }

    move(addAmount, moveChange, moveEnd, moveRestart, moveY) {
        this.x += addAmount * this.velX;
        this.element.style.left = this.x + 'px';
        if (this.name == 'Zero') {
            if (addAmount == -1)
                this.element.style.transform = 'scaleX(-1)';
            else
                this.element.style.transform = 'scaleX(1)';
        }
        else {
            if (addAmount == 1)
                this.element.style.transform = 'scaleX(-1)';
            else
                this.element.style.transform = 'scaleX(1)';
        }
        console.log('hey');
        this.moveStart += moveChange;
        if (this.moveStart <= moveEnd)
            this.moveStart = moveRestart;
        this.element.style.backgroundPosition = this.moveStart + `px ${moveY}px`;
    }

    kenAtk() {
        new Projectile(this.x, this.direction);
    }

    zeroAtk() {
        this.element.style.backgroundImage = "url('r_Zero_Gif.gif')";
        this.element.style.backgroundPosition = '0px 0px';
        this.element.style.backgroundSize = 'cover';
        this.element.style.backgroundRepeat = 'no-repeat';

        console.log(this.y);
        let y = this.y;
        let x = this.x;
        console.log(this.y);

        this.element.style.top = (y - 600) + 'px';
        this.element.style.left = (x - 400) + 'px';
        this.element.style.width = '1000px';
        this.element.style.height = '1000px';
        // this.element.style = '-webkit-transform: scaleX(-1);'

        setTimeout(function () {
            let element = document.getElementById('Zero');
            element.style.backgroundImage = "url('zerox6.gif')";
            element.style.backgroundPosition = '-348px -4100px';
            element.style.backgroundSize = '6000px';
            element.style.width = '300px';
            element.style.height = '350px';
            element.style.backgroundRepeat = 'repeat';
            element.style.top = (y) + 'px';
            element.style.left = (x) + 'px';
            idleInterval = setInterval(function () { stanceZero(zero, 300, -1100, 4075, -45); }, 150);
        }, 4000);
        console.log(this.y);
    }

    attackMove(enemy) {
        console.log(Math.abs(Number(this.x) - Number(enemy.x)));
        if (Math.abs(Number(this.x) - Number(enemy.x)) <= 500) {
            enemy.hp -= 10;
        }
        enemy.healthElement.style.width = enemy.hp + '%';
        console.log(this.name);
        if (this.name === 'Ken')
            this.kenAtk();
        else
            this.zeroAtk();
        if (enemy.hp == 0) {
            alert(`${this.name} wins`);
            location.reload();
        }
        
    }
    
}







